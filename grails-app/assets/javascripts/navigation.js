$(document).ready(function(){
    //$(".navigation-tab:first-child").addClass("active");
    $(".navigation-tab").on("mousedown", function(){
        $(this).addClass("active");
        removeActiveClass($(this))
    });
    var startNavigationOffsetTop = $('#vd-main-navigation').offset().top;
    $(window).scroll(function(){
        var scrollTop     = $(window).scrollTop(),
            elementOffset = $('#vd-main-navigation').offset().top,
            parentWidth = $('#vd-main-navigation').parent().width(),
            distance      = (elementOffset - scrollTop);
        // console.log("window top: "+scrollTop);
        // console.log("element top: "+elementOffset);
        // console.log("parent width: "+parentWidth);
        // console.log(distance);
        // console.log(startNavigationOffsetTop);
        if(distance <= 0){
            // console.log("adding");
            $('#vd-main-navigation').addClass("fixedNavigation").css("width", parentWidth+"px");
        }
        if(scrollTop <= startNavigationOffsetTop || $(window).width() <= 769){
            // console.log("removing");
            $('#vd-main-navigation').removeClass("fixedNavigation").css("width", parentWidth+"px");
        }
    });
});

function removeActiveClass(element){
    $(".navigation-tab").not($(element)).each(function(){
        if($(this).hasClass("active"))
            $(this).removeClass("active");
    });
}
function closeModal(){
    $('#feedbackModal').modal('hide');
    location.reload();
}