package villadiva

class UserFeedback {
    String name
    String country
    String feedback
    Long avatarId
    Long rating
    Date dateCreated

    static constraints = {
        name nullable: true
        country nullable: true
        avatarId nullable: true
        rating nullable: true
        feedback nullable: true, maxSize: 1000000
        dateCreated nullable: true
    }
}
