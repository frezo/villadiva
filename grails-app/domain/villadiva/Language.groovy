package villadiva

class Language {
    String name

    static constraints = {
        name nullable: true
    }
}
