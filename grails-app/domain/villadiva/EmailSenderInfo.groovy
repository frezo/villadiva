package villadiva

class EmailSenderInfo {
    String name
    String email
    String subject
    String message
    Date dateCreated

    static constraints = {
        name nullable: true
        email nullable: true
        subject nullable: true
        message nullable: true, maxSize: 1000000
        dateCreated nullable: true
    }
}
