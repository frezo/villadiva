package villadiva

import grails.converters.JSON

class VillaDivaController {
    def mailService

    def index() {
        Language language = !Language.list().isEmpty() ? Language.list().first() : new Language(name: "English").save(flush: true)
        if(language.name == null || (language.name != "English" && language.name != "German" && language.name != "Poland")){
            language.name = "English"
            language.save(flush: true)
        }
        return [language: language.name]
    }
    def ajaxGetUserFeedBacks(Long rowsPerPage, Long offsetBy){
        Language language = Language.list().first()
        List<UserFeedback> userFeedbackList = UserFeedback.list([max:rowsPerPage, offset:offsetBy*rowsPerPage, sort: "dateCreated", order: "desc"])
        float itemCountFloat = UserFeedback.count()/rowsPerPage
        int itemCount = itemCountFloat == Math.floor(itemCountFloat) ? Math.floor(itemCountFloat)-1 : Math.floor(itemCountFloat) //for removing one extra (empty) page
        render(template: "/standard/userFeedBackListTemplate",
                model: [userFeedbackList: userFeedbackList,
                        numOfPages      : itemCount,
                        offsetBy        : offsetBy,
                        rowsPerPage     : rowsPerPage,
                        language        : language.name])
    }

    def apartments(){
        Language language = Language.list().first()
        return [language: language.name]
    }

    def contact(){
        Language language = Language.list().first()
        return [language: language.name]
    }

    def services(){
        Language language = Language.list().first()
        return [language: language.name]
    }

    def activities(){
        Language language = Language.list().first()
        return [language: language.name]
    }

    def prices(){
        Language language = Language.list().first()
        return [language: language.name]
    }

    def sendEmailAndSaveData() {

        EmailSenderInfo emailSenderInfo =  new EmailSenderInfo(
                name: params.senderName,
                email: params.fromEmail,
                subject: params.subjectTitle,
                message: params.userMessage,
                dateCreated: new Date()
        )
        if (emailSenderInfo.save(flush: true)){
            try{
                sendMail {
                    to "villadiva.stafilic@gmail.com"
                    subject params.subjectTitle
                    html "<h3>Sent from: "+ params.senderName+"</h3>" +
                            "<h4>Email: "+ params.fromEmail +"</h4>" +
                            "<p>Message: <br>"+ params.userMessage+"</p>"
                }
                render([status:"0", message: "Successfully sent mail and save data!"] as JSON)
            }
            catch(e){
                render([status: 1, message: "Failed to send email."] as JSON)
            }
        }
        else
            render([status: 2, message: "Failed to save data."] as JSON)

    }

    def saveUserFeedback(Long rating, Long avatar){
        def userName
        if(params.userName == "")
            userName = "Anonymous"
        else
            userName = params.userName
        UserFeedback userFeedback =  new UserFeedback(
                name: userName,
                country: params.country,
                feedback: params.feedbackDescription,
                avatarId: avatar,
                rating: rating,
                dateCreated: new Date()
        )
        if (userFeedback.save(flush: true))
            render([status: 0, message: "Successfully saved user feedback!"] as JSON)
        else
            render([status: 1, message: "Failed to save user feedback."] as JSON)
    }

    def ajaxSetLanguage(){
        List<Language> languageList = Language.list()
        if(languageList.isEmpty()) {
            Language language = new Language(name: params.country).save(flush: true)
            render([status: 0, message: "Successfully created new Language field!"] as JSON)
        }
        else {
            def firstElement = languageList.first()
            firstElement.name = params.country
            if(firstElement.save(flush: true))
                render([status: 1, message: "Successfully updated Language field!"] as JSON)
            else
                render([status: 2, message: "Failed to update Language field!"] as JSON)
        }
    }
}
