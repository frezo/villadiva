<g:formRemote name="contact-form"
              url="[controller: 'villaDiva', action: 'sendEmailAndSaveData']"
              onSuccess="clickOnModal()">

    <div id="contact-panel" class="panel panel-primary">
        <div class="contact-heading">
            <h3 id="contact-form-title" class="panel-title text-center">
                <g:if test="${language == "English"}">Contact Us</g:if>
                <g:if test="${language == "German"}">Kontaktieren Sie uns</g:if>
                <g:if test="${language == "Poland"}">Skontaktuj się z nami</g:if>
            </h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="senderName" class="col-sm-2 control-label">
                    <g:if test="${language == "English"}">Name:</g:if>
                    <g:if test="${language == "German"}">Name:</g:if>
                    <g:if test="${language == "Poland"}">Nazwa:</g:if>
                </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="senderName" name="senderName" placeholder="John Doe" required>
                    <i class="form-control-feedback">
                        <i class="fa fa-user input-icon-style"></i>
                    </i>
                </div>
            </div>
            <div class="form-group">
                <label for="fromEmail" class="col-sm-2 control-label">Email:</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="fromEmail" name="fromEmail" placeholder="my.email@example.com" required>
                    <i class="form-control-feedback">
                        <i class="fa fa-envelope input-icon-style"></i>
                    </i>
                </div>
            </div>
            <div class="form-group">
                <label for="subjectTitle" class="col-sm-2 control-label">
                    <g:if test="${language == "English"}">Subject:</g:if>
                    <g:if test="${language == "German"}">Fach:</g:if>
                    <g:if test="${language == "Poland"}">Przedmiot:</g:if>
                </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="subjectTitle" name="subjectTitle" placeholder="Subject/Title" required>
                    <i class="form-control-feedback">
                        <i class="fa fa-tag input-icon-style" aria-hidden="true"></i>
                    </i>
                </div>
            </div>
            <div class="form-group">
                <label for="userMessage" class="col-sm-2 control-label">
                    <g:if test="${language == "English"}">Message:</g:if>
                    <g:if test="${language == "German"}">Nachricht:</g:if>
                    <g:if test="${language == "Poland"}">Wiadomość:</g:if>
                </label>
                <div class="col-sm-10">
                    <textarea id="userMessage" name="userMessage" class="form-control" rows="4" style="max-width: 100%;"></textarea>
                </div>
            </div>
            <div class="form-group no-margin">
                <div class="col-xs-12 col-sm-offset-9 col-sm-3 no-padding">
                    <g:if test="${language == "English"}">
                        <g:submitButton name="sendEmailAndSaveData" class="btn btn-primary confirm-button" value="Send"/>
                    </g:if>
                    <g:if test="${language == "German"}">
                        <g:submitButton name="sendEmailAndSaveData" class="btn btn-primary confirm-button" value="Versenden"/>
                    </g:if>
                    <g:if test="${language == "Poland"}">
                        <g:submitButton name="sendEmailAndSaveData" class="btn btn-primary confirm-button" value="Posłać"/>
                    </g:if>
                </div>
            </div>
        </div>
    </div>
</g:formRemote>
