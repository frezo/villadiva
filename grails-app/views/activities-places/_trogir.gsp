<div class="col-xs-12">
    <h2>Trogir</h2>
</div>
<div class="my-gallery col-xs-12" itemscope itemtype="http://schema.org/ImageGallery">
    <figure class="col-xs-12 col-sm-6 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/trogir-images/4.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/trogir-images/4.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 264px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Antique town gates</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/trogir-images/1.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/trogir-images/1.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Old town Loggia</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/trogir-images/6.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/trogir-images/6.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Defence tower of St. Marc</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/trogir-images/8.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/trogir-images/8.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Palace Cipiko</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/trogir-images/9.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/trogir-images/9.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Island of Čiovo</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/trogir-images/3.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/trogir-images/3.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 264px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Cathedral St. Laurentius</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/trogir-images/2.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/trogir-images/2.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 264px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Stronghold Kamerlengo</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/trogir-images/5.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/trogir-images/5.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 264px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Convent of Dominican</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/trogir-images/7.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/trogir-images/7.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 264px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Pavillon of Gloriette</h4>
        </figcaption>
    </figure>
</div>

