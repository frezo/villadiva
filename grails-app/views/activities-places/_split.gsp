<div class="col-xs-12">
    <h2>Split</h2>
</div>
<div class="my-gallery col-xs-12" itemscope itemtype="http://schema.org/ImageGallery">
    <figure class="col-xs-12 col-sm-6 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/split-images/1.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/split-images/1.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 264px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Palace of Imperator Diokletian</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/split-images/2.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/split-images/2.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Cellar of Diokletian's Palace</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/split-images/3.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/split-images/3.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Peristyl of the old town</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/split-images/4.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/split-images/4.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Cathedral St. Duje</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/split-images/6.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/split-images/6.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Statue of Grgur Ninski</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/split-images/7.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/split-images/7.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Silver Gate</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/split-images/8.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/split-images/8.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Golden Gate</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-12 col-sm-6 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" style="float: right;">
        <a href="${assetPath(src: 'villa-diva-images/split-images/5.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/split-images/5.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 264px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Pjaca</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/split-images/9.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/split-images/9.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Beach Bačvice</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/split-images/10.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/split-images/10.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Beach Žnjan</h4>
        </figcaption>
    </figure>
</div>