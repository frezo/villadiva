<div class="col-xs-12">
    <h2>Kaštel Štafilić</h2>
</div>
<div class="my-gallery col-xs-12" itemscope itemtype="http://schema.org/ImageGallery">
    <figure class="col-xs-12 col-sm-6 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/kastel-stafilic/1.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/kastel-stafilic/1.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 264px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Castle "Rotondo" (1508)</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/kastel-stafilic/2.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/kastel-stafilic/2.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>1500 years old olive tree</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/kastel-stafilic/4.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/kastel-stafilic/4.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Archealogical discoveries in Resnik</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/kastel-stafilic/5.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/kastel-stafilic/5.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Renaissance Church Saint Roko</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/kastel-stafilic/6.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/kastel-stafilic/6.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Biblical garden of Stomorje</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/kastel-stafilic/8.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/kastel-stafilic/8.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Quad/Buggy Adventures</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/kastel-stafilic/9.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/kastel-stafilic/9.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Marina Kaštela</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-12 col-sm-6 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" style="float: right;">
        <a href="${assetPath(src: 'villa-diva-images/kastel-stafilic/3.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/kastel-stafilic/3.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 264px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Tower of Nehaj</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/kastel-stafilic/13.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/kastel-stafilic/13.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Rafting & Kayaking on the river Cetina</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/kastel-stafilic/12.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/kastel-stafilic/12.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Hiking on mountain Kozjak</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/kastel-stafilic/10.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/kastel-stafilic/10.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 264px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Sailing & Surfing</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/kastel-stafilic/11.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/kastel-stafilic/11.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 264px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Diving Center Resnik</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/kastel-stafilic/7.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/kastel-stafilic/7.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 264px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Beach Divulje</h4>
        </figcaption>
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/kastel-stafilic/14.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/kastel-stafilic/14.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 264px;" >
        </a>
        <figcaption class="image-caption">
            <h4>Beach Resnik</h4>
        </figcaption>
    </figure>
</div>