<div class="col-xs-12">
    <h2>
        <g:if test="${language == "English"}">Beach </g:if>
        <g:if test="${language == "German"}">Strand  </g:if>
        <g:if test="${language == "Poland"}">Plaża </g:if>
        Divulje
    </h2>
</div>
<div class="my-gallery col-xs-12" itemscope itemtype="http://schema.org/ImageGallery">
    <figure class="col-xs-12 col-sm-6 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/beach-divulje/2.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/beach-divulje/2.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 264px;" >
        </a>
        %{--<figcaption class="image-caption">--}%
            %{--<h4>Third Thumbnail label</h4>--}%
        %{--</figcaption>--}%
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/beach-divulje/1.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/beach-divulje/1.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        %{--<figcaption class="image-caption">--}%
            %{--<h4>Third Thumbnail label</h4>--}%
        %{--</figcaption>--}%
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/beach-divulje/3.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/beach-divulje/3.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        %{--<figcaption class="image-caption">--}%
            %{--<h4>Third Thumbnail label</h4>--}%
        %{--</figcaption>--}%
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/beach-divulje/4.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/beach-divulje/4.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        %{--<figcaption class="image-caption">--}%
            %{--<h4>Third Thumbnail label</h4>--}%
        %{--</figcaption>--}%
    </figure>
    <figure class="col-xs-6 col-sm-3 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'villa-diva-images/beach-divulje/5.gif')}" class="thumbnail beach-thumbnail" itemprop="contentUrl" data-size="670x500">
            <img src="${assetPath(src: 'villa-diva-images/beach-divulje/5.gif')}" itemprop="thumbnail" alt="" style="width: 100%; max-height: 130px;" >
        </a>
        %{--<figcaption class="image-caption">--}%
            %{--<h4>Third Thumbnail label</h4>--}%
        %{--</figcaption>--}%
    </figure>
</div>