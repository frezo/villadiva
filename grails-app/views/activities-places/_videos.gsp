<div class="col-xs-12">
    <h2>
        <g:if test="${language == "English"}">Video Gallery </g:if>
        <g:if test="${language == "German"}">Video Galeria  </g:if>
        <g:if test="${language == "Poland"}">Video-Galerie </g:if>
    </h2>
</div>
<div class="col-xs-12">
    <div class="col-xs-12 col-sm-4 no-padding">
        <h3>Kaštela</h3>
        <!-- 16:9 aspect ratio -->
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item video-content" src="https://www.youtube.com/embed/spGT5UN-lrA" allowfullscreen="allowfullscreen"></iframe>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 no-padding">
        <h3>Trogir</h3>
        <!-- 16:9 aspect ratio -->
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item video-content" src="https://www.youtube.com/embed/WlziMnLXdSQ" allowfullscreen="allowfullscreen"></iframe>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 no-padding">
        <h3>Split</h3>
        <!-- 16:9 aspect ratio -->
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item video-content" src="https://www.youtube.com/embed/DUiVlqpGXMM" allowfullscreen="allowfullscreen"></iframe>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 no-padding">
        <h3>UMF Split</h3>
        <!-- 16:9 aspect ratio -->
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item video-content" src="https://www.youtube.com/embed/qKH9DpPNCrM" allowfullscreen="allowfullscreen"></iframe>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 no-padding">
        <h3>
            <g:if test="${language == "English"}">Beach </g:if>
            <g:if test="${language == "German"}">Strand  </g:if>
            <g:if test="${language == "Poland"}">Plaża </g:if>
            Resnik
        </h3>
        <!-- 16:9 aspect ratio -->
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item video-content" src="https://www.youtube.com/embed/XbCbUauc7hw" allowfullscreen="allowfullscreen"></iframe>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 no-padding">
        <h3>
            <g:if test="${language == "English"}">Beach </g:if>
            <g:if test="${language == "German"}">Strand  </g:if>
            <g:if test="${language == "Poland"}">Plaża </g:if>
            Divulje
        </h3>
        <!-- 16:9 aspect ratio -->
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item video-content" src="https://www.youtube.com/embed/2v1Yn6UAZVw" allowfullscreen="allowfullscreen"></iframe>
        </div>
    </div>
</div>