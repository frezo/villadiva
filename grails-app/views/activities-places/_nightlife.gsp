<div class="col-xs-12">
    <h2>
        <g:if test="${language == "English"}">Nightlife </g:if>
        <g:if test="${language == "German"}">Nachtleben  </g:if>
        <g:if test="${language == "Poland"}">Nocne życie </g:if>
    </h2>
</div>
<div class="col-xs-12">
    <div class="col-xs-12 no-padding nightlife-club">
        <div class="col-xs-7 col-lg-4 nightlife-title"><i class="fa fa-chevron-right"></i> Vanilla Club</div>
        <div class="col-xs-5 col-lg-3 no-padding">
            <a href="https://www.facebook.com/vanilla.bar.split" target="_blank">
                <div class="nightlife-block face-nightlife"><i class="fa fa-facebook"></i></div>
            </a>
            <a href="http://www.vanilla.hr/" target="_blank">
                <div class="nightlife-block website-nightlife"><i class="fa fa-globe"></i></div>
            </a>
            <a href="https://www.google.hr/maps/place/Vanilla+Club/@43.5218128,16.432035,15z/data=!4m2!3m1!1s0x0:0x714a755e5a4493ba" target="_blank">
                <div class="nightlife-block location-nightlife"><i class="fa fa-map-marker"></i></div>
            </a>
        </div>
    </div>
    <div class="col-xs-12 no-padding nightlife-club">
        <div class="col-xs-7 col-lg-4 nightlife-title"><i class="fa fa-chevron-right"></i> Hemingway Club</div>
        <div class="col-xs-5 col-lg-3 no-padding">
            <a href="https://www.facebook.com/HemingwaybarST" target="_blank">
                <div class="nightlife-block face-nightlife"><i class="fa fa-facebook"></i></div>
            </a>
            <a href="http://www.hemingway.hr/split/" target="_blank">
                <div class="nightlife-block website-nightlife"><i class="fa fa-globe"></i></div>
            </a>
            <a href="https://www.google.hr/maps/place/Hemingway+Bar/@43.518151,16.427843,15z/data=!4m2!3m1!1s0x0:0x380790097291c016" target="_blank">
                <div class="nightlife-block location-nightlife"><i class="fa fa-map-marker"></i></div>
            </a>
        </div>
    </div>
    <div class="col-xs-12 no-padding nightlife-club">
        <div class="col-xs-7 col-lg-4 nightlife-title"><i class="fa fa-chevron-right"></i> Tropic Club</div>
        <div class="col-xs-5 col-lg-3 no-padding">
            <a href="https://www.facebook.com/tropic.club.split/" target="_blank">
                <div class="nightlife-block face-nightlife"><i class="fa fa-facebook"></i></div>
            </a>
            <a class="add-pointer" data-toggle="tooltip" data-placement="bottom" title="No available website.">
                <div class="nightlife-block website-nightlife"><i class="fa fa-globe"></i></div>
            </a>
            <a href="https://www.google.hr/maps/place/Tropic+Club/@43.5018822,16.4485388,15z/data=!4m2!3m1!1s0x0:0xd6a334bb8d458a28" target="_blank">
                <div class="nightlife-block location-nightlife"><i class="fa fa-map-marker"></i></div>
            </a>
        </div>
    </div>
    <div class="col-xs-12 no-padding nightlife-club">
        <div class="col-xs-7 col-lg-4 nightlife-title"><i class="fa fa-chevron-right"></i> O'Hara Club</div>
        <div class="col-xs-5 col-lg-3 no-padding">
            <a href="https://www.facebook.com/OharaMusicClub" target="_blank">
                <div class="nightlife-block face-nightlife"><i class="fa fa-facebook"></i></div>
            </a>
            <a href="http://www.ohara.hr/" target="_blank">
                <div class="nightlife-block website-nightlife"><i class="fa fa-globe"></i></div>
            </a>
            <a href="https://www.google.hr/maps/place/Klub+O'Hara/@43.4999318,16.4557062,15z/data=!4m2!3m1!1s0x0:0x7aa58b5c55c24e98" target="_blank">
                <div class="nightlife-block location-nightlife"><i class="fa fa-map-marker"></i></div>
            </a>
        </div>
    </div>
    <div class="col-xs-12 no-padding nightlife-club">
        <div class="col-xs-7 col-lg-4 nightlife-title"><i class="fa fa-chevron-right"></i> Academia Gheto Club</div>
        <div class="col-xs-5 col-lg-3 no-padding">
            <a href="https://www.facebook.com/ClubGhetto/" target="_blank">
                <div class="nightlife-block face-nightlife"><i class="fa fa-facebook"></i></div>
            </a>
            <a class="add-pointer" data-toggle="tooltip" data-placement="bottom" title="No available website.">
                <div class="nightlife-block website-nightlife"><i class="fa fa-globe"></i></div>
            </a>
            <a href="https://www.google.hr/maps/place/Academia+Ghetto+Club/@43.5079625,16.4392239,15z/data=!4m2!3m1!1s0x0:0x9d3e4f94f2dcc392" target="_blank">
                <div class="nightlife-block location-nightlife"><i class="fa fa-map-marker"></i></div>
            </a>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>