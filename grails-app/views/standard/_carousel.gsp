<div class="col-xs-12 no-padding">
    <div id="carousel-example-generic" class="carousel slide no-padding no-margin" data-ride="carousel">

        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
            <li data-target="#carousel-example-generic" data-slide-to="4"></li>
            <li data-target="#carousel-example-generic" data-slide-to="5"></li>
            <li data-target="#carousel-example-generic" data-slide-to="6"></li>
        </ol>

        <div class="carousel-inner" role="listbox">
            <g:if test="${language == "English"}">
                <div class="item active">
                    <img src="${assetPath(src: 'villa-diva-images/house-villa-diva.gif')}" alt="Villa Diva Entrance" width="1200" height="550">
                    <div class="image-description text-center">
                        <div class="image-description-text">Welcome to our accommodation!</div>
                    </div>
                </div>
                <div class="item">
                    <img src="${assetPath(src: 'villa-diva-images/pearl-of-dalmatia.gif')}" alt="Another Picture" width="1200" height="550">
                    <div class="image-description text-center">
                        <div class="image-description-text">Spend your holidays in Kaštel Štafilić!</div>
                    </div>
                </div>
                <div class="item">
                    <img src="${assetPath(src: 'villa-diva-images/stafilic.gif')}" alt="Another Picture" width="1200" height="550">
                    <div class="image-description text-center">
                        <div class="image-description-text">Pearl of Dalmatia</div>
                    </div>
                </div>
                <div class="item">
                    <img src="${assetPath(src: 'villa-diva-images/your-place-of-recreation.gif')}" alt="Your Place of Recreation" width="1200" height="550">
                    <div class="image-description text-center">
                        <div class="image-description-text">Your place of recreation!</div>
                    </div>
                </div>
            </g:if>
            <g:if test="${language == "German"}">
                <div class="item active">
                    <img src="${assetPath(src: 'villa-diva-images/house-villa-diva.gif')}" alt="Villa Diva Entrance" width="1200" height="550">
                    <div class="image-description text-center">
                        <div class="image-description-text">Willkommen in unserer Unterkunft!</div>
                    </div>
                </div>
                <div class="item">
                    <img src="${assetPath(src: 'villa-diva-images/pearl-of-dalmatia.gif')}" alt="Another Picture" width="1200" height="550">
                    <div class="image-description text-center">
                        <div class="image-description-text">Verbringen Sie Ihren Urlaub in Kastel Stafilic!</div>
                    </div>
                </div>
                <div class="item">
                    <img src="${assetPath(src: 'villa-diva-images/stafilic.gif')}" alt="Another Picture" width="1200" height="550">
                    <div class="image-description text-center">
                        <div class="image-description-text">Dalmatiens Perle</div>
                    </div>
                </div>
                <div class="item">
                    <img src="${assetPath(src: 'villa-diva-images/your-place-of-recreation.gif')}" alt="Your Place of Recreation" width="1200" height="550">
                    <div class="image-description text-center">
                        <div class="image-description-text">Hier erholen Sie sich!</div>
                    </div>
                </div>
            </g:if>
            <g:if test="${language == "Poland"}">
                <div class="item active">
                    <img src="${assetPath(src: 'villa-diva-images/house-villa-diva.gif')}" alt="Villa Diva Entrance" width="1200" height="550">
                    <div class="image-description text-center">
                        <div class="image-description-text">Serdecznie witamy w naszym domu!</div>
                    </div>
                </div>
                <div class="item">
                    <img src="${assetPath(src: 'villa-diva-images/pearl-of-dalmatia.gif')}" alt="Another Picture" width="1200" height="550">
                    <div class="image-description text-center">
                        <div class="image-description-text">Spędźcie swój wypoczynek w Kaštel Štafilić!</div>
                    </div>
                </div>
                <div class="item">
                    <img src="${assetPath(src: 'villa-diva-images/stafilic.gif')}" alt="Another Picture" width="1200" height="550">
                    <div class="image-description text-center">
                        <div class="image-description-text">Perła Dalmacji</div>
                    </div>
                </div>
                <div class="item">
                    <img src="${assetPath(src: 'villa-diva-images/your-place-of-recreation.gif')}" alt="Your Place of Recreation" width="1200" height="550">
                    <div class="image-description text-center">
                        <div class="image-description-text">Tutaj państwo odpoczną!</div>
                    </div>
                </div>
            </g:if>

            <div class="item">
                <img src="${assetPath(src: 'villa-diva-images/kastel-stafilic-slikice.png')}" alt="Kastel Stafilic" width="1200" height="550">
                <div class="image-description text-center">
                    <div class="image-description-text">Kaštel Štafilić</div>
                </div>
            </div>
            <div class="item">
                <img src="${assetPath(src: 'villa-diva-images/trogir-slikice.png')}" alt="Trogir" width="1200" height="550">
                <div class="image-description text-center">
                    <div class="image-description-text">Trogir</div>
                </div>
            </div>
            <div class="item">
                <img src="${assetPath(src: 'villa-diva-images/split-slikice.png')}" alt="Split" width="1200" height="550">
                <div class="image-description text-center">
                    <div class="image-description-text">Split</div>
                </div>
            </div>
        </div>

        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>