<div id="fixed-info-bar" class="text-center no-padding" >
    <div class="fixed-line-info col-xs-12">
        <img class="fixed-info-icons" src="${assetPath(src: 'fixed-side-images/wifi.png')}" title="FREE WIFI">
    </div>
    <div class="fixed-line-info col-xs-12">
        <img class="fixed-info-icons" src="${assetPath(src: 'fixed-side-images/Satellite-TV.png')}" title="TV-SAT">
    </div>
    <div class="fixed-line-info col-xs-12">
        <img class="fixed-info-icons" src="${assetPath(src: 'fixed-side-images/air-conditioning.png')}" title="AIR-CONDITIONING">
    </div>
    <div class="fixed-line-info col-xs-12">
        <img class="fixed-info-icons" src="${assetPath(src: 'fixed-side-images/parking.png')}" title="FREE PARKING">
    </div>
    <div class="fixed-line-info col-xs-12">
        <img class="fixed-info-icons" src="${assetPath(src: 'fixed-side-images/barbecue.png')}" title="BARBECUE">
    </div>
    <div class="fixed-line-info col-xs-12">
        <img class="fixed-info-icons" src="${assetPath(src: 'fixed-side-images/beach-umbrella.png')}" title="BEACH">
    </div>
    <div class="fixed-line-info col-xs-12">
        <img class="fixed-info-icons" src="${assetPath(src: 'fixed-side-images/garden.png')}" title="GARDEN">
    </div>
    <div class="fixed-line-info col-xs-12">
        <img class="fixed-info-icons" src="${assetPath(src: 'fixed-side-images/bicycle.png')}" title="RENT A BICYCLE">
    </div>
</div>

<script>
    $("#fixed-info-bar").click(function(){
       if($(this).css("border-right-color") == "rgb(162, 160, 160)"){
           $(this).animate({
               "width": "45px"
           }).css("border-right", "none").addClass("fix-animated");
       } else {
           if($(this).hasClass("fix-animated")){
               $(this).animate({
                   "width": "10px"
               }).css("border-right", "10px solid rgb(162, 160, 160)");
           }
       }
    });
</script>