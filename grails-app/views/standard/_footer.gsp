<div id="vd-footer" class="col-xs-12 no-padding">
    <div id="footer-content" class="col-xs-12 no-padding">
        <g:if test="${language == "English"}">
            <div class="col-xs-6 footer-facilities">
                <h3 class="text-center">Surrounding Facilities</h3><br/>
                <div class="col-xs-12 col-md-4 col-lg-4 footer-facilities-column">
                    <ul class="no-padding">
                        <li><i class="fa fa-chevron-right"></i>Beach:<p>250 m</p></li>
                        <li><i class="fa fa-chevron-right"></i>Downtown:<p>1 km</p></li>
                        <li><i class="fa fa-chevron-right"></i> Groceries: <p>20 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Restaurants: <p>300 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Nightlife: <p>3 km</p></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-4 col-lg-4 footer-facilities-column">
                    <ul class="no-padding">
                        <li><i class="fa fa-chevron-right"></i> Airport: <p>6.5 km</p></li>
                        <li><i class="fa fa-chevron-right"></i> Bus station: <p>30 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Marina: <p>2 km</p></li>
                        <li><i class="fa fa-chevron-right"></i> Rent-a-car: <p>300 m</p></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-4 col-lg-4 footer-facilities-column">
                    <ul class="no-padding">
                        <li><i class="fa fa-chevron-right"></i> Doctor: <p>1 km</p></li>
                        <li><i class="fa fa-chevron-right"></i> Dentist: <p>60 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Pharmacy: <p>50 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Bank: <p>900 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Post Office: <p>900 m</p></li>
                    </ul>
                </div>
            </div>
        </g:if>
        <g:if test="${language == "German"}">
            <div class="col-xs-6 footer-facilities">
                <h3 class="text-center">Infrastruktur</h3><br/>
                <div class="col-xs-12 col-md-4 col-lg-4 footer-facilities-column">
                    <ul class="no-padding">
                        <li><i class="fa fa-chevron-right"></i> Strand:<p>250 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Innenstadt:<p>1 km</p></li>
                        <li><i class="fa fa-chevron-right"></i> Lebensmitteläden: <p>20 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Restaurants: <p>300 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Nachtleben: <p>3 km</p></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-4 col-lg-4 footer-facilities-column">
                    <ul class="no-padding">
                        <li><i class="fa fa-chevron-right"></i> Flughafen: <p>6.5 km</p></li>
                        <li><i class="fa fa-chevron-right"></i> Bussstation: <p>30 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Yachthafen: <p>2 km</p></li>
                        <li><i class="fa fa-chevron-right"></i> Autovermietung: <p>300 m</p></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-4 col-lg-4 footer-facilities-column">
                    <ul class="no-padding">
                        <li><i class="fa fa-chevron-right"></i> Arzt: <p>1 km</p></li>
                        <li><i class="fa fa-chevron-right"></i> Zahnarzt: <p>60 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Apotheke: <p>50 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Bank: <p>900 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Postannahmestelle: <p>900 m</p></li>
                    </ul>
                </div>
            </div>
        </g:if>
        <g:if test="${language == "Poland"}">
            <div class="col-xs-6 footer-facilities">
                <h3 class="text-center">Infrastruktura</h3><br/>
                <div class="col-xs-12 col-md-4 col-lg-4 footer-facilities-column">
                    <ul class="no-padding">
                        <li><i class="fa fa-chevron-right"></i> Plaża:<p>250 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Centrum:<p>1 km</p></li>
                        <li><i class="fa fa-chevron-right"></i> Sklepy spożywcze : <p>20 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Restauracje: <p>300 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Życie nocne: <p>3 km</p></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-4 col-lg-4 footer-facilities-column">
                    <ul class="no-padding">
                        <li><i class="fa fa-chevron-right"></i> Lotnisko: <p>6.5 km</p></li>
                        <li><i class="fa fa-chevron-right"></i> Przystanek autobusowy: <p>30 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Port jachtowy : <p>2 km</p></li>
                        <li><i class="fa fa-chevron-right"></i> Wynajem samochodów: <p>300 m</p></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-4 col-lg-4 footer-facilities-column">
                    <ul class="no-padding">
                        <li><i class="fa fa-chevron-right"></i> Lekarz ogólny: <p>1 km</p></li>
                        <li><i class="fa fa-chevron-right"></i> Dentysta: <p>60 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Apteka: <p>50 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Bank: <p>900 m</p></li>
                        <li><i class="fa fa-chevron-right"></i> Poczta: <p>900 m</p></li>
                    </ul>
                </div>
            </div>
        </g:if>
        <div class="col-xs-6 footer-contact-us">
            <div class="col-xs-12 no-padding">
                <h3 class="text-center">
                    <g:if test="${language == "English"}">Contact Us</g:if>
                    <g:if test="${language == "German"}">Kontaktieren Sie uns</g:if>
                    <g:if test="${language == "Poland"}">Skontaktuj się z nami</g:if>
                </h3>
                <div class="col-xs-12 no-padding">
                    <div class="col-xs-12 col-md-4 col-lg-4 footer-contact-info">
                        <h5><i class="fa fa-map-marker"></i>
                            <g:if test="${language == "English"}">Address:</g:if>
                            <g:if test="${language == "German"}">Adresse:</g:if>
                            <g:if test="${language == "Poland"}">Adres: </g:if>
                        </h5>
                        <span>Ante Casottija 2, 21216 Kaštel Štafilić,
                        <g:if test="${language == "English"}">Croatia</g:if>
                        <g:if test="${language == "German"}">Kroatien</g:if>
                        <g:if test="${language == "Poland"}">Chorwacja</g:if>
                        </span>
                    </div>
                    <div class="col-xs-12 col-md-4 col-lg-4 footer-contact-info">
                        <h5><i class="fa fa-phone"></i>
                            <g:if test="${language == "English"}">Phone:</g:if>
                            <g:if test="${language == "German"}">Mobiltelefon:</g:if>
                            <g:if test="${language == "Poland"}">Telefon:</g:if>
                        </h5>
                        <span>+385 91 971 6574</span>
                    </div>
                    <div class="col-xs-12 col-md-4 col-lg-4 footer-contact-info">
                        <h5><i class="fa fa-envelope-o"></i> Email:</h5>
                        <span>villadiva.stafilic@gmail.com</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 no-padding">
                <h3 class="text-center">
                    <g:if test="${language == "English"}">Feedback</g:if>
                    <g:if test="${language == "German"}">Beurteilung der Villa Diva</g:if>
                    <g:if test="${language == "Poland"}">Opinia o Villa Divas</g:if>
                </h3>
                <div class="col-xs-12 no-padding">
                    <div id="footer-feedback" class="col-xs-12 col-sm-12 no-padding">
                        <span class="col-xs-12 col-md-9 no-padding">
                            <g:if test="${language == "English"}">Visited our Villa Diva? Please, feel free to leave your </g:if>
                            <g:if test="${language == "German"}">Villa Diva besucht ? Hier geht es zur </g:if>
                            <g:if test="${language == "Poland"}">Odwiedziłeś Villa Divas? Napisz proszę swoją </g:if>
                        </span>
                        <div class="col-xs-12 col-md-3 no-padding">
                            <button class=" btn btn-primary feedback-button" data-toggle="modal" data-target="#feedbackModal">
                                <g:if test="${language == "English"}">Feedback</g:if>
                                <g:if test="${language == "German"}">Beurteilung</g:if>
                                <g:if test="${language == "Poland"}">Opinia</g:if>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div id="footer-rights-reserved" class="col-xs-12 no-padding text-center">
        <div>
            All rights reserved <span>Villa Diva</span>  <br/>
            Designed by <b><i style="color: #fff">Frano Rezo</i></b>
        </div>
    </div>

    <!-- Modal -->
    <div id="feedback-form" class="col-xs-12 col-lg-8 col-lg-offset-2 no-padding form-horizontal">
        <g:render template="/standard/feedback-form" model="[language: language]"></g:render>
    </div>

    </div>
</div>

