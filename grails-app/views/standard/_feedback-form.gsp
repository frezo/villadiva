<g:formRemote name="feedback-form"
              url="[controller: 'villaDiva', action: 'saveUserFeedback']"
              onSuccess="closeModal()">
    <div class="modal fade" id="feedbackModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">
                        <i class="fa fa-comment-o" aria-hidden="true" style="padding-right: 10px; font-size: 22px;"></i>
                        <g:if test="${language == "English"}">Feedback</g:if>
                        <g:if test="${language == "German"}">Beurteilung</g:if>
                        <g:if test="${language == "Poland"}">Opinia</g:if>
                    </h4>
                </div>
                <div class="modal-body col-xs-12">
                    <div class="form-group col-xs-12 no-padding feedback-group-form">
                        <label for="userName" class="col-sm-2 control-label" style="padding: 5px 15px;">
                            <g:if test="${language == "English"}">Name:</g:if>
                            <g:if test="${language == "German"}">Name:</g:if>
                            <g:if test="${language == "Poland"}">Nazwa:</g:if>
                        </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="userName" name="userName" placeholder="John Doe" required>
                            <i class="form-control-feedback">
                                <i class="fa fa-user input-icon-style"></i>
                            </i>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 no-padding feedback-group-form">
                        <label for="country" class="col-sm-2 control-label" style="padding: 5px 15px;">
                            <g:if test="${language == "English"}">Country:</g:if>
                            <g:if test="${language == "German"}">Land:</g:if>
                            <g:if test="${language == "Poland"}">Kraj:</g:if>
                        </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="country" name="country" placeholder="Country" required>
                            <i class="form-control-feedback">
                                <i class="fa fa-globe input-icon-style" aria-hidden="true"></i>
                            </i>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 no-padding feedback-group-form">
                        <label class="col-sm-2 control-label" style="padding: 5px 15px;">
                            <g:if test="${language == "English"}">Avatar:</g:if>
                            <g:if test="${language == "German"}">Avatar:</g:if>
                            <g:if test="${language == "Poland"}">Awatara:</g:if>
                        </label>
                        <div class="col-sm-10">
                            <g:each in="${1..12}" var="i">
                                <img class="avatar-image" src="${assetPath(src: 'avatars/avatar'+i+'.gif')}" alt="avatar${i}" name="avatarId" width="50" height="50">
                                <input type="radio" name="avatar" value="${i}" style="display: none;"/>
                            </g:each>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 no-padding feedback-group-form">
                        <label for="feedbackDescription" class="col-sm-2 control-label">
                            <g:if test="${language == "English"}">Description:</g:if>
                            <g:if test="${language == "German"}">Beschreibung:</g:if>
                            <g:if test="${language == "Poland"}">Opis:</g:if>
                        </label>
                        <div class="col-sm-10">
                            <textarea id="feedbackDescription" name="feedbackDescription" class="form-control" rows="4" style="max-width: 100%;" required></textarea>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 no-padding feedback-group-form">
                        <label class="col-sm-2 control-label">
                            <g:if test="${language == "English"}">Rating:</g:if>
                            <g:if test="${language == "German"}">Wertung:</g:if>
                            <g:if test="${language == "Poland"}">Ocena:</g:if>
                        </label>
                        <div class="col-sm-10">
                            <fieldset class="rating rating-feedback">
                                <input type="radio" id="star10" name="rating" value="10" /><label class = "full" for="star10" title="Awesome - 5 stars"></label>
                                <input type="radio" id="star9" name="rating" value="9" /><label class="half" for="star9" title="Pretty good - 4.5 stars"></label>
                                <input type="radio" id="star8" name="rating" value="8" /><label class = "full" for="star8" title="Pretty good - 4 stars"></label>
                                <input type="radio" id="star7" name="rating" value="7" /><label class="half" for="star7" title="Meh - 3.5 stars"></label>
                                <input type="radio" id="star6" name="rating" value="6" /><label class = "full" for="star6" title="Meh - 3 stars"></label>
                                <input type="radio" id="star5" name="rating" value="5" /><label class="half" for="star5" title="Kinda bad - 2.5 stars"></label>
                                <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Kinda bad - 2 stars"></label>
                                <input type="radio" id="star3" name="rating" value="3" /><label class="half" for="star3" title="Meh - 1.5 stars"></label>
                                <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Sucks big time - 1 star"></label>
                                <input type="radio" id="star1" name="rating" value="1" /><label class="half" for="star1" title="Sucks big time - 0.5 stars"></label>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="modal-footer col-xs-12">
                    <div class="col-xs-12 no-padding">
                        <div class="button-div col-xs-12 col-sm-6">
                            <button type="button" class="btn btn-default confirm-button" data-dismiss="modal" style="background-color: #f9f9f9;">
                                <g:if test="${language == "English"}">Close</g:if>
                                <g:if test="${language == "German"}">Schließen</g:if>
                                <g:if test="${language == "Poland"}">Zamknąć</g:if>
                            </button>
                        </div>
                        <div class="button-div col-xs-12 col-sm-6">
                            <g:if test="${language == "English"}">
                                <g:submitButton name="sendFeedback" id="sendFeedback" class="btn btn-primary confirm-button" value="Send Feedback"/>
                            </g:if>
                            <g:if test="${language == "German"}">
                                <g:submitButton name="sendFeedback" id="sendFeedback" class="btn btn-primary confirm-button" value="Feedback abschicken"/>
                            </g:if>
                            <g:if test="${language == "Poland"}">
                                <g:submitButton name="sendFeedback" id="sendFeedback" class="btn btn-primary confirm-button" value="Wyślij opinię"/>
                            </g:if>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</g:formRemote>
<script>
    $(".avatar-image").click(function(){
        $(this).addClass("avatar-image-active").next().click();
        $(".avatar-image").not(this).each(function(){
            if($(this).hasClass("avatar-image-active"))
                $(this).removeClass("avatar-image-active");
        });
    })
</script>