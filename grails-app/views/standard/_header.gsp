<div id="vd-main-header" class="col-xs-12 no-padding">
    <div id="header-lang" class="col-xs-12 no-padding">
        %{--<span>Place for right vocation</span>--}%
        <div class="col-xs-12 no-padding">
            <g:if test="${language == "English"}">
                <div class="col-xs-8 no-padding" style="height: 40px;">
                    <div id="header-lang-description" class="text-adjust">
                        Your Croatian place of recreation
                        <span class="header-title-description">Villa Diva</span>
                    </div>
                </div>
                <div class="col-xs-4 no-padding">
                    <div class="icon-flags-div text-center add-pointer" title="English">
                        <img class="flag-icons" src="${assetPath(src: 'eng_flag.png')}" data-country="English">
                        <div class="hidden-xs inline-block-display flag-icons-text">English</div>
                    </div>
                    <div class="icon-flags-div text-center add-pointer" title="German">
                        <img class="flag-icons" src="${assetPath(src: 'german_flag.png')}" data-country="German">
                        <div class="hidden-xs inline-block-display flag-icons-text">German</div>
                    </div>
                    <div class="icon-flags-div text-center add-pointer" title="Poland">
                        <img class="flag-icons" src="${assetPath(src: 'poland-flag.png')}" data-country="Poland">
                        <div class="hidden-xs inline-block-display flag-icons-text">Poland</div>
                    </div>
                </div>
            </g:if>
            <g:if test="${language == "German"}">
                <div class="col-xs-8 no-padding" style="height: 40px;">
                    <div id="header-lang-description" class="text-adjust">
                        Hier erholen Sie sich in Kroatien
                        <span class="header-title-description">Villa Diva</span>
                    </div>
                </div>
                <div class="col-xs-4 no-padding">
                    <div class="icon-flags-div text-center add-pointer" title="English">
                        <img class="flag-icons" src="${assetPath(src: 'eng_flag.png')}" data-country="English">
                        <div class="hidden-xs inline-block-display flag-icons-text">Englisch</div>
                    </div>
                    <div class="icon-flags-div text-center add-pointer" title="German">
                        <img class="flag-icons" src="${assetPath(src: 'german_flag.png')}" data-country="German">
                        <div class="hidden-xs inline-block-display flag-icons-text">Deutsch</div>
                    </div>
                    <div class="icon-flags-div text-center add-pointer" title="Poland">
                        <img class="flag-icons" src="${assetPath(src: 'poland-flag.png')}" data-country="Poland">
                        <div class="hidden-xs inline-block-display flag-icons-text">Polnisch</div>
                    </div>
                </div>
            </g:if>
            <g:if test="${language == "Poland"}">
                <div class="col-xs-8 no-padding" style="height: 40px;">
                    <div id="header-lang-description" class="text-adjust">
                        Jesteś chorwacki miejsce rekreacji
                        <span class="header-title-description">Villa Diva</span>
                    </div>
                </div>
                <div class="col-xs-4 no-padding">
                    <div class="icon-flags-div text-center add-pointer" title="English">
                        <img class="flag-icons" src="${assetPath(src: 'eng_flag.png')}" data-country="English">
                        <div class="hidden-xs inline-block-display flag-icons-text">Angielski</div>
                    </div>
                    <div class="icon-flags-div text-center add-pointer" title="German">
                        <img class="flag-icons" src="${assetPath(src: 'german_flag.png')}" data-country="German">
                        <div class="hidden-xs inline-block-display flag-icons-text">Niemiecki</div>
                    </div>
                    <div class="icon-flags-div text-center add-pointer" title="Poland">
                        <img class="flag-icons" src="${assetPath(src: 'poland-flag.png')}" data-country="Poland">
                        <div class="hidden-xs inline-block-display flag-icons-text">Polski</div>
                    </div>
                </div>
            </g:if>
        </div>
    </div>
    <div id="header-title" class="col-xs-12 no-padding">
        <img src="${assetPath(src: 'villa-diva-images/villa-diva-cover.gif')}" alt="Villa Diva Cover">
    </div>
</div>
<script>
    $(".icon-flags-div").click(function(){
        var element = $(this);
        var country = $(this).children("img").data("country");
        $.ajax({
            dataType:'json',
            data:{country: country},
            url:"${g.createLink(controller: "villaDiva", action: 'ajaxSetLanguage')}",
            success:function(data){
                console.log(data);
                $(".icon-flags-div").not(element).each(function(){
                    if($(this).hasClass("active"))
                        $(this).removeClass("active");
                });
                $(element).addClass("active");
                location.reload();
            }
        });
    })
</script>