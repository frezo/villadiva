<div class="col-xs-12 no-padding" style="background-color: #ECECEC;">
    <h1 id="feedback-title" class="text-center">
        <g:if test="${language == "English"}">Customer Feedbacks</g:if>
        <g:if test="${language == "German"}">Kundenfeedbacks</g:if>
        <g:if test="${language == "Poland"}">Reakcje klientów</g:if>
    </h1>
    <g:each in="${userFeedbackList}" var="userFeedback">
        <div class="col-xs-12 col-sm-6" style="padding: 20px;">
            <div class="media">
                <div class="media-left">
                    <g:if test="${userFeedback?.avatarId != null}">
                        <img src="${assetPath(src: 'avatars/avatar'+userFeedback?.avatarId+'.gif')}"
                             class="media-object img-circle customer-image" alt="64x64" width="64" height="64" data-holder-rendered="true">
                    </g:if>
                    <g:else>
                        <img src="${assetPath(src: 'user-image.png')}" class="media-object img-circle customer-image" alt="64x64" width="64" height="64" data-holder-rendered="true">
                    </g:else>
                </div>
                <div class="media-body">
                    <div class="user-arrow"></div>
                    <h4 class="media-heading customer-info">${userFeedback?.name}, <span>${userFeedback?.country}</span></h4>
                    <p>${raw(userFeedback?.feedback?.replace("\n", "<br>"))}</p>
                    <h4 class="user-rating">
                        <g:if test="${language == "English"}">Rating:</g:if>
                        <g:if test="${language == "German"}">Wertung:</g:if>
                        <g:if test="${language == "Poland"}">Ocena:</g:if>
                    </h4>
                    <fieldset class="rating" id="rating${userFeedback?.id}">
                        <input type="radio" id="star10-${userFeedback?.id}" name="rating${userFeedback?.id}" value="10" disabled/><label class = "full" for="star10-${userFeedback?.id}" title="Awesome - 5 stars"></label>
                        <input type="radio" id="star9-${userFeedback?.id}" name="rating${userFeedback?.id}" value="9" disabled/><label class="half" for="star9-${userFeedback?.id}" title="Pretty good - 4.5 stars"></label>
                        <input type="radio" id="star8-${userFeedback?.id}" name="rating${userFeedback?.id}" value="8" disabled/><label class = "full" for="star8-${userFeedback?.id}" title="Pretty good - 4 stars"></label>
                        <input type="radio" id="star7-${userFeedback?.id}" name="rating${userFeedback?.id}" value="7" disabled/><label class="half" for="star7-${userFeedback?.id}" title="Meh - 3.5 stars"></label>
                        <input type="radio" id="star6-${userFeedback?.id}" name="rating${userFeedback?.id}" value="6" disabled/><label class = "full" for="star6-${userFeedback?.id}" title="Meh - 3 stars"></label>
                        <input type="radio" id="star5-${userFeedback?.id}" name="rating${userFeedback?.id}" value="5" disabled/><label class="half" for="star5-${userFeedback?.id}" title="Kinda bad - 2.5 stars"></label>
                        <input type="radio" id="star4-${userFeedback?.id}" name="rating${userFeedback?.id}" value="4" disabled/><label class = "full" for="star4-${userFeedback?.id}" title="Kinda bad - 2 stars"></label>
                        <input type="radio" id="star3-${userFeedback?.id}" name="rating${userFeedback?.id}" value="3" disabled/><label class="half" for="star3-${userFeedback?.id}" title="Meh - 1.5 stars"></label>
                        <input type="radio" id="star2-${userFeedback?.id}" name="rating${userFeedback?.id}" value="2" disabled/><label class = "full" for="star2-${userFeedback?.id}" title="Sucks big time - 1 star"></label>
                        <input type="radio" id="star1-${userFeedback?.id}" name="rating${userFeedback?.id}" value="1" disabled/><label class="half" for="star1-${userFeedback?.id}" title="Sucks big time - 0.5 stars"></label>
                    </fieldset>
                </div>
            </div>
        </div>
    </g:each>
    <div id="page-selection" class="col-xs-12 text-center">
        <g:if test="${numOfPages > 0}">
            <ul class="pagination" style="cursor:pointer;">
                <g:set var="from" value="${offsetBy-2}"/>
                <g:set var="to" value="${offsetBy+2}"/>
                <g:if test="${from < 0}">
                    <g:set var="from" value="${0}"/>
                </g:if>
                <g:if test="${to > numOfPages}">
                    <g:set var="to" value="${numOfPages}"/>
                </g:if>


                <li class="${from == 0 && offsetBy == 0 ? 'disabled': '' }">
                    <a class="${from == 0 && offsetBy == 0 ? 'disabled': '' }" onclick="updatePagination(${offsetBy}-1)" >
                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                    </a>
                </li>


                <g:while test="${from <= to}">
                    <li onclick="updatePagination(${from})" class="${from == offsetBy ? 'active':''}" id="${from }">
                        <a>${++from}</a>
                    </li>
                </g:while>

                <li class="${to == numOfPages && offsetBy == numOfPages ? 'disabled': '' }">
                    <a class="${to == numOfPages && offsetBy == numOfPages ? 'disabled': ''}" onclick="updatePagination(${offsetBy}+1)">
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </a>
                </li>
            </ul>
        </g:if>

    </div>
</div>
%{--<div id="itemsPerPage" class="pagination pull-left">--}%
    %{--<g:select  id="selectRange" name="selectRange"  from="${[5, 10,15,25,50]}" value="${userSettings?.rowsPerPage}" class="form-control"  />--}%
%{--</div>--}%

<script>
    $(".disabled").removeAttr("onclick");

    <g:each in="${userFeedbackList}" var="userFeedback">
    $(".media .media-body #rating${userFeedback?.id} input").each(function(){
        <g:if test="${userFeedback?.rating}">
        if($(this).val()== ${userFeedback?.rating})
            $(this).prop('checked', true);
        </g:if>
    });
    </g:each>
</script>