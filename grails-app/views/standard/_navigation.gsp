<div id="vd-main-navigation" class="no-padding col-xs-12">
    <nav class="navbar-default no-border no-border-radius col-xs-12 no-padding" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigationbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="col-xs-12 collapse navbar-collapse no-padding" id="navigationbar">
            <ul id="vd-navigation-list" class="nav navbar-nav no-padding no-margin">
                <li id="navigation-tab-1" class="col-xs-2 no-padding navigation-tab text-center" title="Home">
                    <a href="${createLink(controller: "villaDiva", action: 'index')}">
                    <i class="fa fa-home"></i> Home</a>
                </li>
                <li id="navigation-tab-2" class="col-xs-2 no-padding navigation-tab text-center" title="Apartments">
                    <a href="${createLink(controller: "villaDiva", action: 'apartments')}">
                    <i class="fa fa-bed"></i>
                        <g:if test="${language == "English"}">Apartments</g:if>
                        <g:if test="${language == "German"}">Unterkunft</g:if>
                        <g:if test="${language == "Poland"}">Mieszkania</g:if>
                    </a>
                </li>
                <li id="navigation-tab-3" class="col-xs-2 no-padding navigation-tab text-center" title="Services">
                    <a href="${createLink(controller: "villaDiva", action: 'services')}">
                        <i class="fa fa-magic"></i>
                        <g:if test="${language == "English"}">Services</g:if>
                        <g:if test="${language == "German"}">Leistungen </g:if>
                        <g:if test="${language == "Poland"}">Usługi </g:if>
                    </a>
                </li>
                <li id="navigation-tab-4" role="presentation" class="col-xs-2 no-padding dropdown navigation-tab text-center">
                    <a id="dropdown-tab-prevent" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="Activities">
                        <i class="fa fa-child"></i>
                        <g:if test="${language == "English"}">Activities</g:if>
                        <g:if test="${language == "German"}">Aktivitäten  </g:if>
                        <g:if test="${language == "Poland"}">Aktywność </g:if>
                        <i class="fa fa-caret-down" style="padding-left: 5px;"></i>
                    </a>
                    <ul class="dropdown-menu col-xs-12 no-padding">
                        <li class="text-center no-padding no-margin" title="Beach Resnik">
                            <a href="${createLink(controller: "villaDiva", action: 'activities')}#beach-divulje-images">
                                <g:if test="${language == "English"}">Beach </g:if>
                                <g:if test="${language == "German"}">Strand  </g:if>
                                <g:if test="${language == "Poland"}">Plaża </g:if>
                                Divulje
                            </a>
                        </li>
                        <li class="text-center no-padding no-margin" title="Beach Divulje">
                            <a href="${createLink(controller: "villaDiva", action: 'activities')}#beach-resnik-images">
                                <g:if test="${language == "English"}">Beach </g:if>
                                <g:if test="${language == "German"}">Strand  </g:if>
                                <g:if test="${language == "Poland"}">Plaża </g:if>
                                Resnik
                            </a>
                        </li>
                        <li class="text-center no-padding no-margin" title="Kaštela">
                            <a href="${createLink(controller: "villaDiva", action: 'activities')}#kastel-stafilic-images">Kaštel Štafilić</a>
                        </li>
                        <li class="text-center no-padding no-margin" title="Trogir">
                            <a href="${createLink(controller: "villaDiva", action: 'activities')}#trogir-images">Trogir</a>
                        </li>
                        <li class="text-center no-padding no-margin" title="Split">
                            <a href="${createLink(controller: "villaDiva", action: 'activities')}#split-images">Split</a>
                        </li>
                        <li class="text-center no-padding no-margin" title="Split">
                            <a href="${createLink(controller: "villaDiva", action: 'activities')}#nightlife">
                                <g:if test="${language == "English"}">Nightlife </g:if>
                                <g:if test="${language == "German"}">Nachtleben  </g:if>
                                <g:if test="${language == "Poland"}">Nocne życie </g:if>
                            </a>
                        </li>
                        <li class="text-center no-padding no-margin" title="Split">
                            <a href="${createLink(controller: "villaDiva", action: 'activities')}#video-gallery">
                                <g:if test="${language == "English"}">Video Gallery </g:if>
                                <g:if test="${language == "German"}">Video Galeria  </g:if>
                                <g:if test="${language == "Poland"}">Video-Galerie </g:if>
                            </a>
                        </li>
                    </ul>
                </li>
                <li id="navigation-tab-5" class="col-xs-2 no-padding navigation-tab text-center" title="Prices">
                    <a href="${createLink(controller: "villaDiva", action: 'prices')}">
                        <i class="fa fa-eur"></i>
                        <g:if test="${language == "English"}">Prices </g:if>
                        <g:if test="${language == "German"}">Preise  </g:if>
                        <g:if test="${language == "Poland"}">Ceny </g:if>
                    </a>
                </li>
                <li id="navigation-tab-6" class="col-xs-2 no-padding navigation-tab text-center" title="Contact">
                    <a href="${createLink(controller: "villaDiva", action: 'contact')}">
                        <i class="fa fa-map-marker"></i>
                        <g:if test="${language == "English"}">Contact </g:if>
                        <g:if test="${language == "German"}">Kontakt  </g:if>
                        <g:if test="${language == "Poland"}">Kontakt </g:if>
                    </a>
                </li>

            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</div>
<script>
    $('.navigation-tab, .navigation-tab.dropdown .dropdown-menu li').not(".navigation-tab.dropdown").on('click', function(){
        $('.navbar-collapse').collapse('hide');
    });
    $("#navigationbar ul li a[href^='#']").on('click', function(e) {

        // prevent default anchor click behavior
        e.preventDefault();

        // store hash
        var hash = this.hash;

        // animate
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 300, function(){

            // when done, add hash to url
            // (default click behaviour)
            window.location.hash = hash;
        });

    });
</script>