<%--
  Created by IntelliJ IDEA.
  User: Frano
  Date: 3.4.2016.
  Time: 1:03
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Villa Diva - Home</title>
</head>
<body>
    <div class="col-xs-12 page-content">
        <g:if test="${language == "English"}">
            <h1 class="col-xs-12 text-center">Activities</h1>
            <div class="col-xs-12 no-padding content-holder">
                Besides having fun at the just 250 m far away beach in Kaštel Štafilić,
                there are much more offered alternative tourist features and sportive activities.
            </div>
        </g:if>
        <g:if test="${language == "German"}">
            <h1 class="col-xs-12 text-center">Aktivitäten </h1>
            <div class="col-xs-12 no-padding content-holder">
                Neben dem nur 250 m entfernten und großzügig platzbietenden Strand in Kastel Stafilic,
                können Sie noch zahlreiche weitere kulturelle, historische Besonderheiten in unserem
                Kastela-Raum entdecken und sich ebenfalls sportlich betätigen.
            </div>
        </g:if>
        <g:if test="${language == "Poland"}">
            <h1 class="col-xs-12 text-center">Aktywność</h1>
            <div class="col-xs-12 no-padding content-holder">
                Oprócz plaży w Kastel Stafilic, oferującej dużo przestrzeni, oddalonej o 250 metrów od Villa Divas,
                czekają na Ciebie liczne atrakcje kulturalne i zabytki historyczne
                w rejonie Kastela oraz możliwości korzystania z atrakcji sportowych.
            </div>
        </g:if>
        <div id="beach-divulje-images" class="col-xs-12 places-content">
            <g:render template="/activities-places/beach-divulje" model="[language: language]"></g:render>
        </div>

        <div id="beach-resnik-images" class="col-xs-12 places-content">
            <g:render template="/activities-places/beach-resnik" model="[language: language]"></g:render>
        </div>

        <div id="kastel-stafilic-images" class="col-xs-12 places-content">
            <g:render template="/activities-places/kastel-stafilic" model="[language: language]"></g:render>
        </div>

        <div id="trogir-images" class="col-xs-12 places-content">
            <g:render template="/activities-places/trogir" model="[language: language]"></g:render>
        </div>

        <div id="split-images" class="col-xs-12 places-content">
            <g:render template="/activities-places/split" model="[language: language]"></g:render>
        </div>

        <div id="nightlife" class="col-xs-12 places-content">
            <g:render template="/activities-places/nightlife" model="[language: language]"></g:render>
        </div>

        <div id="video-gallery" class="col-xs-12 places-content">
            <g:render template="/activities-places/videos" model="[language: language]"></g:render>
        </div>

    </div>

    <g:render template="/standard/photoswipe-slideshow"></g:render>

<script>
    $(document).ready(function(){
        $("#navigation-tab-4").addClass("active");
    });
    initPhotoSwipeFromDOM('.my-gallery');
</script>
</body>
</html>