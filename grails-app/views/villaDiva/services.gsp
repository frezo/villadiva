<%--
  Created by IntelliJ IDEA.
  User: Frano
  Date: 3.4.2016.
  Time: 1:02
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Villa Diva - Home</title>
</head>
<body>
    <div class="col-xs-12 page-content">
        <g:if test="${language == "English"}">
            <h1 class="text-center">Services</h1>
            <div class="col-xs-12 no-padding content-holder">
                <div class="col-xs-12 col-lg-6 no-padding">
                    In addition to a modern furnished apartment, our guests are provided by:
                    <ul class="services-list">
                        <li>
                            <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/air-conditioning.png')}" title="AIR-CONDITIONING">
                            <i class="fa fa-arrow-right"></i> Air Conditioner
                        </li>
                        <li>
                            <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/Satellite-TV.png')}" title="TV-SAT">
                            <i class="fa fa-arrow-right"></i> Satellite LED TV
                        </li>
                        <li>
                            <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/wifi.png')}" title="FREE WIFI">
                            <i class="fa fa-arrow-right"></i> 24h Wireless Internet
                        </li>
                        <li>
                            <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/parking.png')}" title="FREE PARKING">
                            <i class="fa fa-arrow-right"></i> Free & Save Parking
                        </li>
                        <li>
                            <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/barbecue.png')}" title="BARBECUE">
                            <i class="fa fa-arrow-right"></i> Place for BBQ
                        </li>
                        <li>
                            <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/beach-umbrella.png')}" title="BALCONY FURNITURE">
                            <i class="fa fa-arrow-right"></i> Furniture on Balcony
                        </li>
                        <li>
                            <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/garden.png')}" title="RENT A BICYCLE">
                            <i class="fa fa-arrow-right"></i> Garden
                        </li>
                        <li>
                            <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/bicycle.png')}" title="RENT A BICYCLE">
                            <i class="fa fa-arrow-right"></i> Rent a Bicycle (on request)
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12 col-lg-6 no-padding">
                    <blockquote>
                        <i class="fa fa-plane"></i>
                        <i style="padding-right: 5px; text-decoration: underline;">Note:</i>
                        Villa Diva's manager is always at our guest disposal and is pleased
                        to offer <b><i>free shuttle to the airport</i></b>.
                        <p></p>
                    </blockquote>
                </div>
            </div>
        </g:if>
        <g:if test="${language == "German"}">
            <h1 class="text-center">Leistungen</h1>
            <div class="col-xs-12 no-padding content-holder">
                <div class="col-xs-12 col-lg-6 no-padding">
                    Zusätzlich zu einem modern möbilierten Apartment, sind unsere Gäste versorgt mit:
                    <ul class="services-list">
                        <li>
                            <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/air-conditioning.png')}" title="AIR-CONDITIONING">
                            <i class="fa fa-arrow-right"></i> Klimanlage
                        </li>
                        <li>
                            <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/Satellite-TV.png')}" title="TV-SAT">
                            <i class="fa fa-arrow-right"></i> LED - Satellitenfernsehen
                        </li>
                        <li>
                            <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/wifi.png')}" title="FREE WIFI">
                            <i class="fa fa-arrow-right"></i> 24 h WLAN
                        </li>
                        <li>
                            <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/parking.png')}" title="FREE PARKING">
                            <i class="fa fa-arrow-right"></i> freier und sicherer Parkplatz
                        </li>
                        <li>
                            <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/barbecue.png')}" title="BARBECUE">
                            <i class="fa fa-arrow-right"></i> Grillplatz
                        </li>
                        <li>
                            <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/beach-umbrella.png')}" title="BALCONY FURNITURE">
                            <i class="fa fa-arrow-right"></i> Möbel auf dem Balkon
                        </li>
                        <li>
                            <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/garden.png')}" title="RENT A BICYCLE">
                            <i class="fa fa-arrow-right"></i> Garten
                        </li>
                        <li>
                            <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/bicycle.png')}" title="RENT A BICYCLE">
                            <i class="fa fa-arrow-right"></i> Fahrradausleih (auf Nachfrage)
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12 col-lg-6 no-padding">
                    <blockquote>
                        <i class="fa fa-plane"></i>
                        <i style="padding-right: 5px; text-decoration: underline;">Anmerkung:</i>
                        Villa Divas Manager steht immer unseren Gästen zur Verfügung und
                        bietet einen <b><i>kostenlosen Transport vom und zum Flughafen an</i></b>.
                        <p></p>
                    </blockquote>
                </div>
            </div>
        </g:if>
        <g:if test="${language == "Poland"}">
            <h1 class="text-center">Usługi</h1>
            <div class="col-xs-12 no-padding content-holder">
                <div class="col-xs-12 col-lg-6 no-padding">
                Oprócz nowocześnie urządzonego apartamentu, nasi goście mają do dyspozycji:
                <ul class="services-list">
                    <li>
                        <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/air-conditioning.png')}" title="AIR-CONDITIONING">
                        <i class="fa fa-arrow-right"></i> Klimatyzację
                    </li>
                    <li>
                        <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/Satellite-TV.png')}" title="TV-SAT">
                        <i class="fa fa-arrow-right"></i> Telewizję satelitarną LED
                    </li>
                    <li>
                        <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/wifi.png')}" title="FREE WIFI">
                        <i class="fa fa-arrow-right"></i> 24-godziny dostęp do Wi-Fi
                    </li>
                    <li>
                        <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/parking.png')}" title="FREE PARKING">
                        <i class="fa fa-arrow-right"></i> Bezpłatny i bezpieczny parking
                    </li>
                    <li>
                        <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/barbecue.png')}" title="BARBECUE">
                        <i class="fa fa-arrow-right"></i> Grill
                    </li>
                    <li>
                        <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/beach-umbrella.png')}" title="BALCONY FURNITURE">
                        <i class="fa fa-arrow-right"></i> Umeblowany balkon
                    </li>
                    <li>
                        <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/garden.png')}" title="RENT A BICYCLE">
                        <i class="fa fa-arrow-right"></i> Ogród
                    </li>
                    <li>
                        <img class="services-list-item" src="${assetPath(src: 'fixed-side-images/bicycle.png')}" title="RENT A BICYCLE">
                        <i class="fa fa-arrow-right"></i> Wypożyczalnia rowerów (na życzenie)
                    </li>
                </ul>
            </div>
                <div class="col-xs-12 col-lg-6 no-padding">
                    <blockquote>
                        <i class="fa fa-plane"></i>
                        <i style="padding-right: 5px; text-decoration: underline;">Adnotacja:</i>
                        Manager Villa Divas jest zawsze do dyspozycji gości i
                        oferuje <b><i>bezpłatny transport z i na lotnisko</i></b>.
                        <p></p>
                    </blockquote>
                </div>
            </div>
        </g:if>
    </div>
<script>
    $(document).ready(function(){
        $("#navigation-tab-3").addClass("active");
//        removeActiveClass($(this));
    });
</script>
</body>
</html>