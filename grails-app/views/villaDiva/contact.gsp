<%--
  Created by IntelliJ IDEA.
  User: Frano
  Date: 3.4.2016.
  Time: 1:02
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Villa Diva - Home</title>
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script>
        var myCenter=new google.maps.LatLng(43.54698897022325,16.322239637374878);

        function initialize()
        {
            var mapProp = {
                center: myCenter,
                zoom:11,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

            var marker = new google.maps.Marker({
                position: myCenter,
                title:'Click to zoom'
            });

            marker.setMap(map);

            // Zoom to 9 when clicking on marker
            google.maps.event.addListener(marker,'click',function() {
                map.setZoom(15);
                map.setCenter(marker.getPosition());
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</head>
<body>
    <div class="col-xs-12 page-content">
        <g:if test="${language == "English"}">
            <h1 class="text-center">Location</h1>
            <div class="col-xs-12 no-padding content-holder">
                <div class="col-xs-12 col-sm-4">
                    Villa Diva's apartments are located in Kaštel Štafilić,
                    a settlement between Trogir and Split. <br>
                    Just 2 km from the airport in the direction of Split, you will find us
                    following street of Dr. Franjo Tuđman. <br>
                    In the final step turning into street Bile. After 20 meters you will see on the
                    right side of the street your accommodation - <b><i>Villa Diva!</i></b> <br> <br>
                    We are always available to explain you how to arrive at Villa Diva!
                </div>
                <div id="googleMap" class="col-xs-12 col-sm-8" style="height:380px;"></div>
            </div>
        </g:if>
        <g:if test="${language == "German"}">
            <h1 class="text-center">Lage</h1>
            <div class="col-xs-12 no-padding content-holder">
                <div class="col-xs-12 col-sm-4">
                    Villa Divas Apartments befinden sich im kleinen Ort Kastel Stafilic,
                    das zwischen Trogir und Split lokalisiert ist.<br>
                    Nur 2 km vom Flughafen entfernt, werden Sie uns erreichen, indem Sie der Hauptstraße "Dr. Franjo Tudjman" in Richtung Split folgen. <br>
                    Im vorletzten Schritt biegen Sie in die Nebenstraße "Bile" ein.
                    Nach ca. 20 Metern haben Sie ihr Ziel-<b><i>Villa Diva </i></b>erreicht. <br> <br>
                    Für noch genauere Erläuterungen, stehen wir Ihnen immer gerne zur Verfügung.
                </div>
                <div id="googleMap" class="col-xs-12 col-sm-8" style="height:380px;"></div>
            </div>
        </g:if>
        <g:if test="${language == "Poland"}">
            <h1 class="text-center">Lokalizacja</h1>
            <div class="col-xs-12 no-padding content-holder">
                <div class="col-xs-12 col-sm-4">
                    Apartamenty Villa Divas znajdują się w małej miejscowości Kastel Stafilic,
                    która leży pomiędzy Strogir a Split.<br>
                    Kastel Stafilic jest oddalone zaledwie dwa kilometry od lotniska. Można dotrzeć do nas kierując się  główną drogą "Dr. Franjo Tudjman"<br>
                    w kierunku Split i skręcić w boczną drogę "Bile". Po 20 metrach zobaczą państwo swój cel <b><i>Villa Divas</i></b>. <br> <br>
                    W celu uzyskania bardziej szczegółowych informacji,  zawsze chętnie służymy pomocą.
                </div>
                <div id="googleMap" class="col-xs-12 col-sm-8" style="height:380px;"></div>
            </div>
        </g:if>

        %{-- ----------CONTACT FORM--------------- --}%
        <div id="contact-form" class="col-xs-12 col-lg-8 col-lg-offset-2 no-padding form-horizontal">
            <g:render template="/contact-form/contactForm" model="[language: language]"></g:render>
        </div>

    </div>



<!-- Button trigger modal -->
<button type="button" id="myModalButton" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" style="display: none;">
    Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    Your message has been successfully sent!
                </h4>
            </div>
            <div class="modal-body">
                Thank you for contacting us! We'll answer You as soon as possible.
            </div>
            %{--<div class="modal-footer">--}%
                %{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}%
                %{--<button type="button" class="btn btn-primary">Save changes</button>--}%
            %{--</div>--}%
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#navigation-tab-6").addClass("active");

        $("#myModal").focusout(function(){
            location.reload();
        });
    });

    function clickOnModal() {
        console.log("in function");
        $("#myModalButton").click();
    }
    function callSendingMail(){
        var contactForm = $("#contact-form")[0];
        if(contactForm.checkValidity()){
            jQuery.ajax({
                dataType: 'json',
                data: {
                    name: $("#name").val(),
                    fromEmail: $("#fromEmail").val(),
                    subjectTitle: $("#subjectTitle").val(),
                    mailText: $("#userMessage").val()
                },
                url: "${g.createLink(controller: 'villaDiva', action: 'sendEmailAndSaveData')}",
                success: function (data) {
                    console.log(data);
                }
            });
        }
    }
</script>
</body>
</html>