<%--
  Created by IntelliJ IDEA.
  User: Frano
  Date: 6.3.2016.
  Time: 23:40
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Villa Diva - Home</title>
</head>
<body>
    %{--<g:hiddenField name="chosenLanguage" id="chosenLanguage" value="${language}"></g:hiddenField>--}%
    <div class="col-xs-12 no-padding">
        <g:render template="/standard/carousel"></g:render>
    </div>
    <g:if test="${!userFeedbackList?.isEmpty()}">
        <div id="allFeedBacks"></div>
    </g:if>


<g:hiddenField name="offsetBy" id="offsetBy" value="0"></g:hiddenField>
<g:hiddenField name="rowsPerPage" id="rowsPerPage" value="2"></g:hiddenField>
<script>
    $(document).ready(function(){
        $("#navigation-tab-1").addClass("active");
        showUserFeedbacks();

        <g:each in="${userFeedbackList}" var="userFeedback">
            $(".media .media-body #rating${userFeedback?.id} input").each(function(){
                <g:if test="${userFeedback?.rating}">
                    if($(this).val()== ${userFeedback?.rating})
                        $(this).prop('checked', true);
                </g:if>
            });
        </g:each>
    });


    function updatePagination(offsetBy)
    {
        $('#offsetBy').val(offsetBy);
        showUserFeedbacks();
    }
    function showUserFeedbacks()
    {
        $.ajax({
            dataType:'html',
            data:{
                rowsPerPage: $('#rowsPerPage').val(),
                offsetBy: $("#offsetBy").val()
            },
            url:"${g.createLink(controller: 'villaDiva', action: 'ajaxGetUserFeedBacks')}",
            success:function(data){
                $("#allFeedBacks").html(data);
            }
        });
    }
</script>
</body>
</html>