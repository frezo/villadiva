<div class="my-gallery col-xs-12" itemscope itemtype="http://schema.org/ImageGallery" style="padding-top: 20px;">
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/dnevni1.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/dnevni1.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/dnevni2.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/dnevni2.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/dnevni3.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/dnevni3.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/kauc1.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/kauc1.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/mali-dnevni2.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/mali-dnevni2.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/mali-dnevni3.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/mali-dnevni3.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/mali-dnevni4.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/mali-dnevni4.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/kuhinja1.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/kuhinja1.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/hodnik1.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/hodnik1.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/hodnik2.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/hodnik2.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/hodnik3.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/hodnik3.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/sobe1.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/sobe1.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/sobe2.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/sobe2.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/sobe4.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/sobe4.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/sobe5.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/sobe5.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/sobe7.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/sobe7.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/wc1.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/wc1.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/wc2.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/wc2.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/wc3.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/wc3.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/wc4.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/wc4.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/wc5.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/wc5.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/vanka1.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/vanka1.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/kamin.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/kamin.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>
    <figure class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/vertical/izvana.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="900x1600">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/vertical/izvana.gif')}" itemprop="thumbnail" alt="No image" width="400" height="650">
        </a>
    </figure>

    %{-- horizontal images --}%
    <figure class="col-xs-12 col-sm-6 col-md-4 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/horizontal/kauc2.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="1600x900">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/horizontal/kauc2.gif')}" itemprop="thumbnail" alt="No image" width="700" height="400">
        </a>
    </figure>
    <figure class="col-xs-12 col-sm-6 col-md-4 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/horizontal/kuhinja2.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="1600x900">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/horizontal/kuhinja2.gif')}" itemprop="thumbnail" alt="No image" width="700" height="400">
        </a>
    </figure>
    <figure class="col-xs-12 col-sm-6 col-md-4 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/horizontal/mali-dnevni1.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="1600x900">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/horizontal/mali-dnevni1.gif')}" itemprop="thumbnail" alt="No image" width="700" height="400">
        </a>
    </figure>
    <figure class="col-xs-12 col-sm-6 col-md-4 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/horizontal/mali-dnevni5.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="1600x900">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/horizontal/mali-dnevni5.gif')}" itemprop="thumbnail" alt="No image" width="700" height="400">
        </a>
    </figure>
    <figure class="col-xs-12 col-sm-6 col-md-4 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/horizontal/sobe3.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="1600x900">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/horizontal/sobe3.gif')}" itemprop="thumbnail" alt="No image" width="700" height="400">
        </a>
    </figure>
    <figure class="col-xs-12 col-sm-6 col-md-4 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/horizontal/sobe6.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="1600x900">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/horizontal/sobe6.gif')}" itemprop="thumbnail" alt="No image" width="700" height="400">
        </a>
    </figure>
    <figure class="col-xs-12 col-sm-6 col-md-4 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/horizontal/room-inside1.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="1600x900">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/horizontal/room-inside1.gif')}" itemprop="thumbnail" alt="No image" width="700" height="400">
        </a>
    </figure>
    <figure class="col-xs-12 col-sm-6 col-md-4 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/horizontal/room-inside2.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="1600x900">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/horizontal/room-inside2.gif')}" itemprop="thumbnail" alt="No image" width="700" height="400">
        </a>
    </figure>
    <figure class="col-xs-12 col-sm-6 col-md-4 no-padding" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="${assetPath(src: 'apartments-images/horizontal/room-table1.gif')}" class="thumbnail apartment-images" itemprop="contentUrl" data-size="1600x900">
            <img class="apart-image" src="${assetPath(src: 'apartments-images/horizontal/room-table1.gif')}" itemprop="thumbnail" alt="No image" width="700" height="400">
        </a>
    </figure>
</div>