<%--
  Created by IntelliJ IDEA.
  User: Frano
  Date: 3.4.2016.
  Time: 0:24
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Villa Diva - Home</title>
</head>
<body>
    <div class="col-xs-12 page-content">
        <g:if test="${language == "English"}">
            <h1 class="text-center">Apartments</h1>
            <div class="col-xs-12 no-padding content-holder">
                <div class="col-xs-12">
                    Feel free & do not hesitate to contact us to find for your vacations a suitable apartment.
                    <br> Villa Diva's apartments achieve completely high quality three stars standards.
                    <br> Themed by mission of offering unique & unforgettable accommodation, every apartment
                is equipped with modern kitchens and bathrooms, comfortable beds and last, but not least,
                spacious balconies with an incredible sea view.
                </div>
                <g:render template="apartments-images"></g:render>
            </div>
        </g:if>
        <g:if test="${language == "German"}">
            <h1 class="text-center">Unterkunft</h1>
            <div class="col-xs-12 no-padding content-holder">
                <div class="col-xs-12">
                    Zögern Sie nicht uns zu kontaktieren und für Sie das passende Apartment für ihren Kroatienurlaub in Kastel Stafilic zu finden.
                    <br>Die mietbaren Apartments in der Villa Diva erfüllen vollkommen die normierten 3 Sterne Standarde und bieten sogar viel mehr.
                    <br>Angetrieben von der Mission und erfüllt vom Willen eine einzigartige und unvergessliche Unterkunft unseren Gästen anzubieten,
                    wurden unsere Apartments mit modernsten Küchen und Bädern, komfortablen Betten, größräumigen Balkons ausgestattet. Nicht zu vergessen
                    ist der wunderschöne Ausblick auf das nur 250 m entfernte Meer.
                </div>
                <g:render template="apartments-images"></g:render>
            </div>
        </g:if>
        <g:if test="${language == "Poland"}">
            <h1 class="text-center">Mieszkania</h1>
            <div class="col-xs-12 no-padding content-holder">
                <div class="col-xs-12">
                    Nie wahaj się z nami skontaktować, aby znaleźć dla siebie odpowiedni apartament na wakacje w Kastel Stafilic, w Chorwacji.
                    <br>Apartamenty do wynajęcia w Villa Diva spełniają nie tylko 3 gwiazdkowe standarty, ale oferują znacznie więcej.
                    <br>Pragnąc zaoferować naszym gościom wyjątkowy i niezapomniany pobyt,
                    apartamenty zostały wyposażone w nowoczesne kuchnie i łazienki, wygodne łóżka oraz w przestronne balkony, nie zapominając również
                    o pięknym widoku na morze, które jest oddalone zaledwie o 250 metrów.
                </div>
                <g:render template="apartments-images"></g:render>
            </div>
        </g:if>
        <g:render template="/standard/photoswipe-slideshow"></g:render>
    </div>

<script>
    $(document).ready(function(){
        $("#navigation-tab-2").addClass("active");
    });
    initPhotoSwipeFromDOM('.my-gallery');

</script>
</body>
</html>