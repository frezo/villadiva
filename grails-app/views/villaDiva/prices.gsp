<%--
  Created by IntelliJ IDEA.
  User: Frano
  Date: 7.4.2016.
  Time: 2:17
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Villa Diva - Home</title>
</head>
<body>
<div class="col-xs-12 page-content">
    <g:if test="${language == "English"}">
        <h1 class="text-center">Prices</h1>
        <div class="col-xs-12 no-padding content-holder">
            <div class="col-xs-12">
                Villa Diva's apartments are more than an accommodation. <br>
                In Villa Diva guests are treated as like family members. <br>
                Our wish is to make yourself feel like at home at the first day of stay in our beautiful country of Croatia.<br>
                Despite of offered high quality apartments, the prices are much more than hospitably and customer-friendly.<br>
                Providing confidence in our guest, Villa Diva's manager ist not requiring pre-payments or any other extra
                taxes during your stay at our accommodation! <br>
            </div>
            <div class="col-xs-12 no-padding" style="margin-top: 20px;">
                <table class="table table-bordered text-center" style="box-shadow: 0 1px 10px rgba(0, 0, 0, 0.50);">
                    <thead style="background-color: #3E4149; color: #fff;">
                    <tr>
                        <th class="text-center price-header">
                            <div class="col-xs-12 no-padding price-title">Preseason</div>
                            <div class="col-xs-12 no-padding price-date">(01.01. - 16.06.)</div>
                        </th>
                        <th class="text-center">
                            <div class="col-xs-12 no-padding price-title">High Season</div>
                            <div class="col-xs-12 no-padding price-date">(17.06. - 01.09.)</div>
                        </th>
                        <th class="text-center">
                            <div class="col-xs-12 no-padding price-title">Late Season</div>
                            <div class="col-xs-12 no-padding price-date">(02.09. - 31.12.)</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <div class="col-xs-12 no-padding price-value">21 €</div>
                            <div class="col-xs-12 no-padding">per person & day</div>
                        </td>
                        <td>
                            <div class="col-xs-12 no-padding price-value">24 €</div>
                            <div class="col-xs-12 no-padding">per person & day</div>
                        </td>
                        <td>
                            <div class="col-xs-12 no-padding price-value">21 €</div>
                            <div class="col-xs-12 no-padding">per person & day</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-xs-12 no-padding">
                <blockquote>
                    <h2>Attention!</h2>
                    If you are more than 2 persons and stay more than one week, <br>
                    Villa Diva's Manager will be delighted to offer you an unrefusable discount! <br>
                    Feel free & contact us!
                    <p></p>
                </blockquote>
            </div>
        </div>
    </g:if>
    <g:if test="${language == "German"}">
        <h1 class="text-center">Preise</h1>
        <div class="col-xs-12 no-padding content-holder">
            <div class="col-xs-12">
                Villa Divas Apartments sind mehr als eine Unterkunft. <br>
                Unsere Gäste werden wie Familienangehörige behandelt, denn unser Ziel ist es vom ersten Tag  ihres Urlaubes in Kroatien an,Ihnen
                das Gefühl zu vermitteln zu Hause zu sein. <br>
                Trotz der angebotenen hochwertigen Qualität unserer Unterkunft, sind die Preise fair und kundenfreundlich. <br>
                Auf der Basis eines familiären und freundschaftlichen Vertrauens, werden von Ihnen bei der Ankunft keinesfalls Vorzahlungen oder Zahlungen von zusätzlichen
                Tourismussteuern gefordert. <br>
            </div>
            <div class="col-xs-12 no-padding" style="margin-top: 20px;">
                <table class="table table-bordered text-center" style="box-shadow: 0 1px 10px rgba(0, 0, 0, 0.50);">
                    <thead style="background-color: #3E4149; color: #fff;">
                    <tr>
                        <th class="text-center price-header">
                            <div class="col-xs-12 no-padding price-title">Vorsaison</div>
                            <div class="col-xs-12 no-padding price-date">(01.01. - 16.06.)</div>
                        </th>
                        <th class="text-center">
                            <div class="col-xs-12 no-padding price-title">Hochsaison</div>
                            <div class="col-xs-12 no-padding price-date">(17.06. - 01.09.)</div>
                        </th>
                        <th class="text-center">
                            <div class="col-xs-12 no-padding price-title">Spätsaison</div>
                            <div class="col-xs-12 no-padding price-date">(02.09. - 31.12.)</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <div class="col-xs-12 no-padding price-value">21 €</div>
                            <div class="col-xs-12 no-padding">pro Person & Tag</div>
                        </td>
                        <td>
                            <div class="col-xs-12 no-padding price-value">24 €</div>
                            <div class="col-xs-12 no-padding">pro Person & Tag</div>
                        </td>
                        <td>
                            <div class="col-xs-12 no-padding price-value">21 €</div>
                            <div class="col-xs-12 no-padding">pro Person & Tag</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-xs-12 no-padding">
                <blockquote>
                    <h2>Achtung!</h2>
                    Bei mehr als 2 Personen und einem Urlaub von mehr als einer Woche in unserer Unterkunft, <br>
                    ist unser Manager erfreut Ihnen einen unauschlagbaren Preisnachlass zu geben! <br>
                    Fühlen Sie sich frei & kontaktieren Sie uns!
                    <p></p>
                </blockquote>
            </div>
        </div>
    </g:if>
    <g:if test="${language == "Poland"}">
        <h1 class="text-center">Ceny</h1>
        <div class="col-xs-12 no-padding content-holder">
            <div class="col-xs-12">
                Apartamenty Villa Divas są więcej, aniżeli tylko miejscem noclegowym. <br>
                Nasi goście są traktowani jak członkowie rodziny, gdyż naszym celem jest, abyś od pierwszego dnia swojego pobytu
                na wakacjach w Chorwacji czuł się jak u siebie w domu. <br>
                Pomimo oferowanej wysokiej jakości naszego zakwaterowania, ceny są uczciwe i przyjazne dla klienta. <br>
                Na podstawie rodzinnego i przyjacielskiego zaufania, nie pobieramy od Ciebie żadnej zaliczki ani dodatkowych
                podatków turystycznych po Twoim przyjeździe. <br>
            </div>
            <div class="col-xs-12 no-padding" style="margin-top: 20px;">
                <table class="table table-bordered text-center" style="box-shadow: 0 1px 10px rgba(0, 0, 0, 0.50);">
                    <thead style="background-color: #3E4149; color: #fff;">
                    <tr>
                        <th class="text-center price-header">
                            <div class="col-xs-12 no-padding price-title">Przed sezonem</div>
                            <div class="col-xs-12 no-padding price-date">(01.01. - 16.06.)</div>
                        </th>
                        <th class="text-center">
                            <div class="col-xs-12 no-padding price-title">W sezonie</div>
                            <div class="col-xs-12 no-padding price-date">(17.06. - 01.09.)</div>
                        </th>
                        <th class="text-center">
                            <div class="col-xs-12 no-padding price-title">Koniec sezonu</div>
                            <div class="col-xs-12 no-padding price-date">(02.09. - 31.12.)</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <div class="col-xs-12 no-padding price-value">21 €</div>
                            <div class="col-xs-12 no-padding">na osobę & dzień</div>
                        </td>
                        <td>
                            <div class="col-xs-12 no-padding price-value">24 €</div>
                            <div class="col-xs-12 no-padding">na osobę & dzień</div>
                        </td>
                        <td>
                            <div class="col-xs-12 no-padding price-value">21 €</div>
                            <div class="col-xs-12 no-padding">na osobę & dzień</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-xs-12 no-padding">
                <blockquote>
                    <h2>Uwaga!</h2>
                    Przy liczbie większej niż dwie osoby i urlopie dłuższym niż jeden tydzień w naszym ośrodku, <br>
                    mamy przyjemność zaoferować Tobie nadzwyczajną obniżkę ceny! <br>
                    Nie krępuj się & skontaktuj się z nami!
                    <p></p>
                </blockquote>
            </div>
        </div>
    </g:if>
</div>
<script>
    $(document).ready(function(){
        $("#navigation-tab-5").addClass("active");
//        removeActiveClass($(this));
    });
</script>
</body>
</html>